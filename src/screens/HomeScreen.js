import React, { useState } from 'react';
import { Text, StyleSheet, View, ScrollView, Button, TextInput, Alert } from 'react-native';
import MapView from 'react-native-maps';

const HomeScreen = () => {
  const logoName = 'Ice Cream <3';
  const [name, setName] = useState('');
  const [ph, setPh] = useState('');
  const [msg, setMsg] = useState('');

  return (
    // The outermost view
    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-start' }}
      keyboardShouldPersistTaps='handled' style={styles.topViewStyle}>
      {/* View to justify logo in center */}
      <View style={styles.logoStyle}>
        <Text style={{ fontSize: 20, fontWeight: '500' }}>{logoName}</Text>
      </View>

      {/* view for contact form */}
      <View style={styles.formStyle}>
        {/* name */}
        <Text style={styles.titleStyles}>Name</Text>
        <TextInput
          style={styles.input}
          autoCapitalize='words'
          autoCorrect={false}
          // keyboardType={"numeric"}
          value={name}
          onChangeText={newValue => setName(newValue)}
        />

        {/* phone number field */}
        <Text style={styles.titleStyles}>Phone Number</Text>
        <TextInput
          style={styles.input}
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={"numeric"}
          value={ph}
          maxLength={10}
          onChangeText={newValue => setPh(newValue)}
        />

        {/* validation for ph number */}
        {
          (ph.length < 10 && ph.length >= 1) ? <Text style={{ color: 'red' }}>Phone number must have 10 numbers</Text> : null
        }

        {/* message */}
        <Text style={styles.titleStyles}>Message</Text>
        <TextInput
          style={styles.inputMsg}
          autoCorrect={true}
          value={msg}
          multiline={true}
          onChangeText={newValue => setMsg(newValue)}
        />

        {/* submit button */}
        {name.length >= 1 && ph.length === 10 && msg.length >= 1 ?
          <Button
            style={styles.btnStyle}
            title="Submit"
            disabled={false}
            onPress={() => {
              // console.log("YAY");
              alert("Your query is recorded");
              setMsg('');
              setName('');
              setPh('');
            }} /> :
          <Button
            style={styles.btnStyle}
            title="Submit"
            disabled={true}
            onPress={() => {
              // console.log("NAY");
            }}
          />}


      </View>

      {/* mapview */}
      <MapView
        style={styles.mapStyle}
        region={{
          // Melbourne's longitude latitude
          latitude: -37.8136,
          longitude: 144.9631,
          latitudeDelta: 0.05,
          longitudeDelta: 0.04
        }}

      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  //style for outermost view
  topViewStyle: {
    flex: 1,
    // justifyContent: 'flex-start',
    // alignItems:'stretch', ////by default
  },
  //justifies the logo to center
  logoStyle: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'gray',
    marginTop: 10,

  },
  //adds 20 margin to form
  formStyle: {
    // flex: 0.7,
    marginHorizontal: 20,
    // backgroundColor: 'gray',
  },

  //map style is flexed 1, so it covers all the open space below the form
  mapStyle: {
    flex: 1,
    marginHorizontal: 20,
    marginVertical: 10,
    position: 'relative',
  },
  //bolden titles
  titleStyles: {
    marginTop: 20,
    fontWeight: '500',
  },

  //input fields background color and margin and height
  input: {
    backgroundColor: '#f1f1f1',
    borderColor: 'gray',
    borderBottomWidth: 1,
    height: 30,
  },
  inputMsg: {
    backgroundColor: '#f1f1f1',
    borderColor: 'gray',
    borderBottomWidth: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    textAlignVertical: "bottom",
    height: 70,
  },
});

export default HomeScreen;
