# GoodPairGuysAssessment

First clone the repo, then go inside the directory `goodpairguysassessment` in terminal, <br /> 
then, run the following commands, 
<br /> 
`$ npm install`
<br /> 
`$ npx expo-cli install react-native-gesture-handler react-native-reanimated react-navigation-stack`
<br /> 
`$ npm install --save react-native-maps`
<br /> 
`$ react-native link react-native-maps`
<br /> <br /> 
To start the project,<br /> 
`$ npm start`
<br /> 